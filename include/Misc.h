#ifndef MISC_H
#define MISC_H

#include "MugenEngine.h"

int random ( int beginRange, int endRange );
float pourcent ( float maxValue, float value );
float proportion ( float maxValue, float value, float range );


SDL_Color rgba ( Uint8 r, Uint8 g, Uint8 b, Uint8 a );
void drawLine ( SDL_Renderer * renderer, int x1, int y1, int x2, int y2, SDL_Color color );
void drawRect ( SDL_Renderer * renderer, int x, int y, int w, int h, SDL_Color color );
void drawFillRect ( SDL_Renderer * renderer, int x, int y, int w, int h, SDL_Color color );

void drawGrid ( SDL_Renderer * renderer, int sizeX, int sizeY, SDL_Color color, int screenW, int screenH );

void drawText ( SDL_Renderer* renderer, TTF_Font * font, int x, int y, SDL_Color color, std::string txt );

void drawImage ( SDL_Renderer * renderer, SDL_Surface * surface, int x, int y, int flags );

#endif // MISC_H
