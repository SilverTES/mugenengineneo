#ifndef MUGENENGINE_H
#define MUGENENGINE_H

#include<iostream>
#include<memory>
#include<SDL2/SDL.h>
#include<SDL2/SDL_opengl.h>
#include<SDL2/SDL_image.h>
#include<SDL2/SDL_ttf.h>

#include "Misc.h"
#include "Window.h"


template <class E, class M>
E log ( E error, M msg )
{
#ifdef SHOW_LOG
    std::cout << msg;
#endif // SHOW_LOG
    return error;
}


class MugenEngine
{
    public:
        MugenEngine();
        virtual ~MugenEngine();

        virtual int init() = 0;
        virtual int done() = 0;
        virtual void update() = 0;
        virtual void render() = 0;

        int run();

    protected:
        bool _quit;

    private:

        int initMugenEngine();
        int doneMugenEngine();
        int loopMugenEngine();

        int initAPI();
        int doneAPI();
};


#endif // MUGENENGINE_H
