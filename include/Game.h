#ifndef GAME_H
#define GAME_H

#include "MugenEngine.h"

class Game : public MugenEngine
{
    public:
        int init() override;
        int done() override;
        void update() override;
        void render() override;

    protected:

    private:
        SDL_Event _event;
        bool isFullScreen = false;
        std::shared_ptr<Window> _window;

        int x;

        TTF_Font * _font = NULL;
        SDL_Surface * _sprite = NULL;
        //SDL_GLContext _glcontext;

        int mouseX;
        int mouseY;

        Uint32 fps;
        Uint32 minimumFrameTime;

        Uint32 frameTime;
        Uint32 lastFrameTime;
        Uint32 deltaTime;
};

#endif // GAME_H
