#include "Window.h"

Window::Window()
{
    log ( 0, "- Window Created !\n" );
}

Window::~Window()
{
    log ( 0, "- Window Deleted !\n" );
}
int Window::init ( const char* name,
                   int screenW, int screenH,
                   int scaleWin, int scaleFull,
                   bool fullScreen )
{
    _name = name;
    _screenW = screenW;
    _screenH = screenH;
    _scaleWin = scaleWin;
    _scaleFull = scaleFull;
    _isMaxScale = false;
    _isFullScreen = fullScreen;
    _currentMonitor = 0;

//    SDL_GL_SetAttribute ( SDL_GL_CONTEXT_MAJOR_VERSION, 3 );
//    SDL_GL_SetAttribute ( SDL_GL_CONTEXT_MINOR_VERSION, 2 );
//    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
//    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);

    _windowDisplay = SDL_CreateWindow ( _name,
                                        SDL_WINDOWPOS_CENTERED,
                                        SDL_WINDOWPOS_CENTERED,
                                        _screenW * _scaleWin,
                                        _screenH * _scaleWin,
                                        SDL_WINDOW_SHOWN ); // | SDL_WINDOW_OPENGL );
    if ( !_windowDisplay )
        return log ( 1, "- Unable to create Window !\n" );
    _windowBuffer = SDL_CreateRenderer ( _windowDisplay,
                                         -1,
                                         SDL_RENDERER_ACCELERATED |
                                         SDL_RENDERER_TARGETTEXTURE );// | SDL_RENDERER_PRESENTVSYNC );
    _texBuffer = SDL_CreateTexture ( _windowBuffer,
                                     SDL_PIXELFORMAT_RGBA8888,
                                     SDL_TEXTUREACCESS_TARGET,
                                     _screenW, _screenH );
    pollMonitor ( _currentMonitor );
    SDL_SetRenderDrawBlendMode ( _windowBuffer, SDL_BLENDMODE_BLEND );
    int scale ( 0 );
    _isFullScreen ? scale = scaleFull : scale = scaleWin;
    setWindow ( _windowDisplay, _currentMonitor, _isFullScreen, scale );
    return log ( 0, "- init Window OK !\n" );
}

int Window::done()
{
    SDL_DestroyRenderer ( _windowBuffer );
    SDL_DestroyWindow ( _windowDisplay );
    return log ( 0, "- done Window OK !\n" );
}

void Window::beginRender ( Uint8 r, Uint8 g, Uint8 b, Uint8 a )
{
    SDL_SetRenderTarget ( _windowBuffer, _texBuffer );
    SDL_SetRenderDrawColor ( _windowBuffer, r, g, b, a );
    SDL_RenderClear ( _windowBuffer );
}

void Window::endRender()
{
    SDL_SetRenderTarget ( _windowBuffer, NULL );
    SDL_SetRenderDrawColor ( _windowBuffer, 0, 0, 0, 255 );
    SDL_RenderClear ( _windowBuffer );
    SDL_RenderCopyEx ( _windowBuffer, _texBuffer, NULL, NULL, 0, NULL, SDL_FLIP_NONE );
    SDL_Rect viewRect;
    if ( _isFullScreen )
    {
        viewRect.x = _viewX;
        viewRect.y = _viewY;
    }
    else
    {
        viewRect.x = 0;
        viewRect.y = 0;
    }
    viewRect.w = _viewW;
    viewRect.h = _viewH;
    SDL_RenderSetViewport ( _windowBuffer, &viewRect );
    SDL_RenderPresent ( _windowBuffer );
}

void Window::toggleFullScreen ( int scale )
{
    _isFullScreen = !_isFullScreen;
    setScale ( scale );
}

void Window::switchMonitor ( int scale )
{
    _currentMonitor++;
    if ( _currentMonitor > SDL_GetNumVideoDisplays() - 1 ) _currentMonitor = 0;
    setMonitor ( _currentMonitor, scale ); // -1 scale default , 0 scale Max !
}

void Window::setMonitor ( int monitor, int scale )
{
    // if prev Monitor is on maxScale then new Monitor go maxScale too !
    if ( !_isFullScreen )
        _scaleWin == getMaxScale() - 1 ? _isMaxScale = true : _isMaxScale = false;
    else
        _scaleFull == getMaxScale() ? _isMaxScale = true : _isMaxScale = false;
    _currentMonitor = monitor;
    pollMonitor ( _currentMonitor ); // peek Monitor infos !
    if ( _isFullScreen )
    {
        scale = _scaleFull;
        // if prev Monitor Scale is different than Max scale Monitor possible !
        if ( scale > getMaxScale() || _isMaxScale || scale == 0 )
            scale = getMaxScale();
    }
    else
    {
        scale = _scaleWin;
        // if prev Monitor Scale is different than Max scale Monitor possible !
        if ( scale > getMaxScale() - 1 || _isMaxScale || scale == 0 )
            scale = getMaxScale() - 1;
    }
    setWindow ( _windowDisplay, _currentMonitor, _isFullScreen, scale );
}

void Window::setScale ( int scale )
{
    pollMonitor ( _currentMonitor );
    if ( _isFullScreen )
    {
        if ( scale > getMaxScale() || scale == 0 )
            scale = getMaxScale();
        if ( scale == -1 )
            _scaleFull > getMaxScale() ? scale = getMaxScale() : scale = _scaleFull;
    }
    else
    {
        if ( scale > getMaxScale() - 1 || scale == 0 )
            scale = getMaxScale() - 1;
        if ( scale == -1 )
            _scaleWin > getMaxScale() - 1 ? scale = getMaxScale() - 1 : scale = _scaleWin;
    }
    setWindow ( _windowDisplay, currentMonitor ( _windowDisplay ), _isFullScreen, scale );
}

void Window::setWindow ( SDL_Window* display, int adapter, bool isFullScreen, int scale )
{
    _isFullScreen = isFullScreen;
    _currentMonitor = adapter;
    if ( _isFullScreen )
    {
        //al_set_display_flag(_windowDisplay,ALLEGRO_NOFRAME, true);
        pollMonitor ( adapter ); // peek Monitor infos !
        scale == 0 ? _scaleFull = getMaxScale() : _scaleFull = scale;
        setViewAtCenter ( _scaleFull );
        if ( _currentMonitor != currentMonitor ( _windowDisplay ) )
        {
            SDL_SetWindowFullscreen ( display, 0 );
            //al_set_window_position(display,_currentMonitorX+_viewX,_currentMonitorY+_viewY);
            SDL_SetWindowPosition ( display, _currentMonitorX + _viewX, _currentMonitorY + _viewY );
            //al_resize_display(_windowDisplay,_screenW*_scaleWin,_screenH*_scaleWin);
            SDL_SetWindowSize ( display, _screenW * _scaleWin, _screenH * _scaleWin );
        }
        //al_set_window_position(display,_currentMonitorX,_currentMonitorY);
        //al_resize_display(display,_currentMonitorW,_currentMonitorH);
        SDL_SetWindowFullscreen ( display, SDL_WINDOW_FULLSCREEN_DESKTOP );
//        // Windows System Specifique
//        #ifdef WINSYSTEM
//            HWND hwnd = al_get_win_window_handle(_windowDisplay);
//            SetWindowPos(hwnd, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
//        #endif // WINSYSTEM
    }
    else
    {
        //al_set_display_flag(_windowDisplay,ALLEGRO_NOFRAME, false);
        pollMonitor ( adapter ); // peek Monitor infos !
        scale == 0 ? _scaleWin = getMaxScale() - 1 : _scaleWin = scale;
        setViewAtCenter ( _scaleWin );
        SDL_SetWindowFullscreen ( display, 0 );
        //al_set_window_position(display,_currentMonitorX+_viewX,_currentMonitorY+_viewY);
        SDL_SetWindowPosition ( display, _currentMonitorX + _viewX, _currentMonitorY + _viewY );
        //al_resize_display(_windowDisplay,_screenW*_scaleWin,_screenH*_scaleWin);
        SDL_SetWindowSize ( display, _screenW * _scaleWin, _screenH * _scaleWin );
    }
}

void Window::setViewAtCenter ( int scale )
{
    _viewW = _screenW * scale;
    _viewH = _screenH * scale;
    _viewX = ( _currentMonitorW - _viewW ) / 2;
    _viewY = ( _currentMonitorH - _viewH ) / 2;
}

int Window::getMaxScale()
{
    int ratioX = _currentMonitorW / _screenW;
    int ratioY = _currentMonitorH / _screenH;
    return std::min ( ratioX, ratioY );
}

void Window::pollMonitor ( int adapter )
{
    SDL_Rect info;
    SDL_GetDisplayBounds ( adapter, &info );
    _currentMonitorX = info.x;
    _currentMonitorY = info.y;
    _currentMonitorW = info.w;
    _currentMonitorH = info.h;
}

bool Window::isFullScreen() const
{
    return _isFullScreen;
}

SDL_Window* Window::display()
{
    return _windowDisplay;
}

SDL_Renderer* Window::buffer()
{
    return _windowBuffer;
}
SDL_Texture* Window::texBuffer()
{
    return _texBuffer;
}

int Window::screenW() const
{
    return _screenW;
}

int Window::screenH() const
{
    return _screenH;
}

int Window::centerX() const
{
    return _screenW / 2;
}

int Window::centerY() const
{
    return _screenH / 2;
}

int Window::scaleWin() const
{
    return _scaleWin;
}

int Window::scaleFull() const
{
    return _scaleFull;
}

int Window::viewX() const
{
    return _viewX;
}

int Window::viewY() const
{
    return _viewY;
}

int Window::viewW() const
{
    return _viewW;
}

int Window::viewH() const
{
    return _viewH;
}

int Window::currentMonitor ( SDL_Window* display )
{
    for ( int i ( 0 ); i < SDL_GetNumVideoDisplays() ; i++ )
    {
        SDL_Rect info;
        SDL_GetDisplayBounds ( i, &info );
        int xWin ( 0 );
        int yWin ( 0 );
        SDL_GetWindowPosition ( _windowDisplay, &xWin, &yWin );
        if ( xWin >= info.x &&
                yWin >= info.y &&
                xWin < info.x + info.w &&
                yWin < info.y + info.h )
        {
            _currentMonitor = i;
            return i;
        }
    }
    return 0;
}

int Window::currentMonitor() const
{
    return _currentMonitor;
}

int Window::currentMonitorX() const
{
    return _currentMonitorX;
}

int Window::currentMonitorY() const
{
    return _currentMonitorY;
}

int Window::currentMonitorW() const
{
    return _currentMonitorW;
}

int Window::currentMonitorH() const
{
    return _currentMonitorH;
}

void Window::getMousePos ( int& xMouse, int& yMouse )
{
    SDL_GetMouseState ( &_xMouseWin, &_yMouseWin );
    if ( _isFullScreen )
    {
        xMouse = ( _xMouseWin - _viewX ) / _scaleFull;
        yMouse = ( _yMouseWin - _viewY ) / _scaleFull;
    }
    else
    {
        xMouse = _xMouseWin / _scaleWin;
        yMouse = _yMouseWin / _scaleWin;
    }
    if ( xMouse < 0 )          xMouse = 0;
    if ( xMouse > _screenW - 1 ) xMouse = _screenW - 1;
    if ( yMouse < 0 )          yMouse = 0;
    if ( yMouse > _screenH - 1 ) yMouse = _screenH - 1;
}

int Window::x() const
{
    return _x;
}

int Window::y() const
{
    return _y;
}
