#include "Misc.h"

int random ( int beginRange, int endRange )
{
    return ( rand() % endRange ) + beginRange;
}
float pourcent ( float maxValue, float value )
{
    return ( 100 * value ) / maxValue;
}
float proportion ( float maxValue, float value, float range )
{
    return ( range * value ) / maxValue;
}

SDL_Color rgba ( Uint8 r, Uint8 g, Uint8 b, Uint8 a )
{
    SDL_Color color = {r, g, b, a};
    return color;
}

void drawLine ( SDL_Renderer * renderer, int x1, int y1, int x2, int y2, SDL_Color color )
{
    SDL_SetRenderDrawColor ( renderer, color.r, color.g, color.b, color.a );
    SDL_RenderDrawLine ( renderer, x1, y1, x2, y2 );
}

void drawRect ( SDL_Renderer * renderer, int x, int y, int w, int h, SDL_Color color )
{
    SDL_Rect rect = { x, y, w, h };
    SDL_SetRenderDrawColor ( renderer, color.r, color.g, color.b, color.a );
    SDL_RenderDrawRect ( renderer, &rect );
}

void drawFillRect ( SDL_Renderer * renderer, int x, int y, int w, int h, SDL_Color color )
{
    SDL_Rect rect = { x, y, w, h };
    SDL_SetRenderDrawColor ( renderer, color.r, color.g, color.b, color.a );
    SDL_RenderFillRect ( renderer, &rect );
}

void drawGrid ( SDL_Renderer * renderer, int sizeX, int sizeY, SDL_Color color, int screenW, int screenH )
{
    for ( int i ( 0 ); i < ( screenW / sizeX ); i++ )
    {
        drawLine ( renderer, i * sizeX, 0, i * sizeX, screenH, color );
    }
    for ( int i ( 0 ); i < ( screenH / sizeY ) + 1; i++ )
    {
        drawLine ( renderer, 0, i * sizeY, screenW, i * sizeY, color );
    }
}

void drawText ( SDL_Renderer* renderer,
                TTF_Font * font,
                int x,
                int y,
                SDL_Color color,
                std::string txt )
{
    SDL_Surface * surface = TTF_RenderText_Solid ( font, txt.c_str(), color );
    SDL_Texture * texture = SDL_CreateTextureFromSurface ( renderer, surface );
    SDL_Rect rect;
    SDL_QueryTexture ( texture, NULL, NULL, &rect.w, &rect.h );
    rect.x = x;
    rect.y = y;
    SDL_RenderCopy ( renderer, texture, nullptr, &rect );
    SDL_DestroyTexture ( texture );
    SDL_FreeSurface ( surface );
}

void drawImage ( SDL_Renderer * renderer,
                 SDL_Surface * surface,
                 int x,
                 int y,
                 int flags )
{
    SDL_Texture * texture = SDL_CreateTextureFromSurface ( renderer, surface );
    SDL_Rect rect;
    SDL_QueryTexture ( texture, NULL, NULL, &rect.w, &rect.h );
    rect.x = x;
    rect.y = y;
    SDL_RenderCopy ( renderer, texture, NULL, &rect );
    SDL_DestroyTexture ( texture );
}
