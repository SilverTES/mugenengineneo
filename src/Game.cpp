#include "Game.h"

int Game::init()
{
    _window = std::make_shared<Window>();
    //SDL_SetHint(SDL_HINT_RENDER_VSYNC, "1");
    _window->init ( "-- Mugen Engine NEO -- SDL2 ", 640, 360, 2, 0, false );

    SDL_ShowCursor ( SDL_DISABLE );

    x = 0;

    fps = 60;
    minimumFrameTime = 1000 / fps;
    frameTime = 0;
    lastFrameTime = 0;
    deltaTime = 0;

    _font = TTF_OpenFont ( "data/Kyrou.ttf", 8 );
    if ( _font == NULL )
        return log ( 1, "- Unable to load font !\n" );
    _sprite = IMG_Load ( "data/yoshi.png" );
    if ( _sprite == NULL )
        return log ( 1, "- Unable to load sprite !\n" );


    return log ( 0, "- init Game OK !\n" );
}

int Game::done()
{
    SDL_FreeSurface ( _sprite );
    TTF_CloseFont ( _font );
    _window->done();
    return log ( 0, "- done Game OK !\n" );
}

void Game::update()
{
    if ( ( SDL_GetPerformanceFrequency() - frameTime ) < minimumFrameTime )
        SDL_Delay ( minimumFrameTime - ( SDL_GetPerformanceFrequency() - frameTime ) );
    frameTime = SDL_GetPerformanceFrequency();
    deltaTime = frameTime - lastFrameTime;
    lastFrameTime = frameTime;

    while ( SDL_PollEvent ( &_event ) )
    {
        if ( _event.type == SDL_QUIT )
            _quit = true;
        if ( _event.type == SDL_KEYDOWN )
        {
            if ( _event.key.keysym.sym == SDLK_ESCAPE )
                _quit = true;
            if ( _event.key.keysym.sym == SDLK_SPACE )
            {
                //_window->toggleFullScreen(0);
                _window->toggleFullScreen ( -1 );
                isFullScreen = !isFullScreen;
            }
            if ( _event.key.keysym.sym == SDLK_TAB )
                _window->switchMonitor ( -1 );
            if ( _event.key.keysym.sym == SDLK_0 )
                _window->setScale ( 0 );
            if ( _event.key.keysym.sym == SDLK_1 )
                _window->setScale ( 1 );
            if ( _event.key.keysym.sym == SDLK_2 )
                _window->setScale ( 2 );
            if ( _event.key.keysym.sym == SDLK_3 )
                _window->setScale ( 3 );
            if ( _event.key.keysym.sym == SDLK_4 )
                _window->setScale ( 4 );
        }
    }
    _window->getMousePos ( mouseX, mouseY );

    if ( x > _window->screenW() )
        x = 0;
    else
        x += 1;


}

void Game::render()
{
    _window->beginRender ( 20, 80, 100, 255 );

    drawGrid ( _window->buffer(), 16, 16, rgba ( 40, 100, 140, 150 ), _window->screenW(), _window->screenH() );

    drawFillRect ( _window->buffer(), 10, 10, 50, 50, rgba ( 255, 255, 0, 50 ) );

    drawRect ( _window->buffer(), 0, 0, _window->screenW(), _window->screenH(), rgba ( 255, 255, 0, 150 ) );

    drawText ( _window->buffer(), _font, 40, 40, rgba ( 155, 255, 0, 200 ), "MUGEN WORLD !" );
    drawText ( _window->buffer(), _font, 120, 80, rgba ( 155, 255, 0, 200 ), "YOSHI x = " + std::to_string ( x ) );

    drawImage ( _window->buffer(), _sprite, x, _window->centerY(), 0 );

    drawLine ( _window->buffer(), 0, 0, x,  _window->centerY(), rgba ( 255, 255, 255, 150 ) );

    drawLine ( _window->buffer(),
               0, mouseY,
               _window->screenW(), mouseY,
               rgba ( 55, 255, 255, 150 ) );
    drawLine ( _window->buffer(),
               mouseX, 0,
               mouseX, _window->screenH(),
               rgba ( 55, 255, 255, 150 ) );

    _window->endRender();
}
