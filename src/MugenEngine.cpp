#include "MugenEngine.h"

MugenEngine::MugenEngine()
{
    log ( 0, "- MugenEgine Created !\n" );
}
MugenEngine::~MugenEngine()
{
    log ( 0, "- MugenEgine Deleted !\n" );
}

int MugenEngine::initAPI()
{
    SDL_Init ( SDL_INIT_EVERYTHING );
    IMG_Init ( IMG_INIT_JPG |
               IMG_INIT_PNG |
               IMG_INIT_TIF |
               IMG_INIT_WEBP );
    TTF_Init();
    return log ( 0, "- init API OK !\n" );
}
int MugenEngine::doneAPI()
{
    TTF_Quit();
    IMG_Quit();
    SDL_Quit();
    return log ( 0, "- done API OK !\n" );
}

int MugenEngine::initMugenEngine()
{
    if ( initAPI() )
        return log ( 1, "- init API error !\n" );
    if ( init() )
        return log ( 1, "- init Game error !\n" );
    _quit = false;
    return log ( 0, "- init MugenEngine OK !\n" );
}
int MugenEngine::doneMugenEngine()
{
    if ( done() )
        return log ( 1, "- done Game error !\n" );
    return log ( 0, "- done MugenEngine OK !\n" );
}

int MugenEngine::loopMugenEngine()
{
    while ( !_quit )
    {
        update();
        render();
    }
    return log ( 0, "- loop MugenEngine OK !\n" );
}


int MugenEngine::run()
{
    if ( initMugenEngine() )
        return log ( 1, "- init MugenEngine error !\n" );
    if ( loopMugenEngine() )
        return log ( 2, "- loop MugenEngine error !\n" );
    if ( doneMugenEngine() )
        return log ( 3, "- done MugenEngine error !\n" );
    return log ( 0, "- run MugenEngine OK !\n" );
}
